const keystone = require('keystone');

module.exports = {
    singleQueryItem(itemName, queryObj) {
        return keystone.list(itemName).model.findOne(queryObj,(err,result)=>{
            return new Promise((resolve,reject)=>{
                if (result)
                    resolve(result);
                if (err)
                    reject(err);
            })
        })
    },
    multipleQueryItems(itemName, queryObj){
        return keystone.list(itemName).model.find(queryObj,(err,result)=>{
            return new Promise((resolve,reject)=>{
                if (result)
                    resolve(result);
                if (err)
                    reject(err);
            })
        })
    }
}
