import Vue from 'vue';
import {AxiosService}  from './helpers';

let axiosService: AxiosService = new AxiosService();

let v = new Vue({
    delimiters : ['[[',']]'],
    data: {
        message: "test",
        },
    methods:{
        click() {
            let form:HTMLFormElement = <HTMLFormElement>document.getElementById("contact-us-form");
            let formData:FormData = new FormData(form);
           
            axiosService.postData("/sendmailzz",formData);
            
        },
        goto(idElement: string) {
            if (idElement.startsWith("/")){
                window.location.href = idElement;
                return;
            }

            var container:HTMLElement = this.$el.querySelector(idElement);
            if (!container) {
                console.error(`container ${idElement} doesn't exists`);
                return;
            }
            container.scrollIntoView({
                behavior: 'smooth'
            })
        }
    },
el: "#vue-app"
});
