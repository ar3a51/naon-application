import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export class AxiosService {
    constructor(
        private _baseUrl?:string
    ){
    }

    private displayError(error: any){
        if (error.response){
            console.warn(error.response.header);
            console.warn(error.response.status);
            console.warn(error.response.data);
        } else if (error.request) {
            console.warn(error.request);
        } else {
            console.warn(error.message);
        }
    }

    public  postData(url:string, data: any): any {
        return axios.post(url, data, { baseURL: this._baseUrl})
             .then((result:AxiosResponse)=>{
                return result.data;
             })
             .catch((error)=> {
                this.displayError(error);
             })
    }

    public getData(url: string): any{
        return axios.get(url, {baseURL: this._baseUrl})
            .then((result:AxiosResponse)=> {
                return result.data;
            })
            .catch((error)=> {
                this.displayError(error);
            })
    }       
}