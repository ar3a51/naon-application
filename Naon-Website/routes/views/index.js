var keystone = require('keystone');
const query = require('../../helper/keystoneQuery');

exports = module.exports = async function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;
		var meta = {
		title: "",
	}
	
	

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	
	var home = await query.singleQueryItem('Home');
	var services = await query.multipleQueryItems('Services');
	var servicesSmall = await query.multipleQueryItems('Service-Small');

	locals.title = home.title;
	locals.services = services;
	locals.serviceSmalls = servicesSmall;

	view.render('home', home);
	//view.query('home', Home.model.findOne());
	/*Home.model.findOne({},(err, result)=>{

		locals.title = result.title;
		locals.home = result;
	// Render the view
		view.render('home');
	});*/
};

