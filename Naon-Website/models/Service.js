const keystone = require('keystone');
const Types = keystone.Field.Types;

const Services = new keystone.List('Services');

Services.add({
    title: {type: Types.Text, initial: true, required: true, index: true},
    description: {type: Types.Textarea, initial: true, required: true, index: true},
    date: {type: Types.Datetime, default: Date.now, initial: true, required: true}
});

Services.defaultColumns = 'title, description, date';
Services.register();
