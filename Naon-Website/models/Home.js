const keystone = require('keystone');
const Types = keystone.Field.Types;

const Home = new keystone.List('Home');

Home.add({
    title: {type: Types.Text, initial: true, required: true},
    description: {type: Types.Textarea, initial: true, required: true},
    content: {type: Types.Html, wysiwyg: true, initial:true, required: true},
    service: {type: Types.Html, wysiwyg: true, initial: true},
    date: {type: Types.Datetime, default: Date.now, initial: true, required: true}
});

Home.register();
