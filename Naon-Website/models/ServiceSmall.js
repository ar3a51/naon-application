const keystone = require('keystone');
const Types = keystone.Field.Types;

const ServiceSmall = new keystone.List('Service-Small');

ServiceSmall.add({
    title: {type: Types.Text, initial: true, required: true, index: true},
    description: {type: Types.Textarea, initial: true, required: true, index: true},
    date: {type: Types.Datetime, default: Date.now, initial: true, required: true}
});

ServiceSmall.defaultColumns = 'title, description, date';
ServiceSmall.register();
