module.exports={
    client: {
        service: {
            name: "naon",
            url: `${process.env.VUE_APP_NAON_ENDPOINT}/graphql`
        }
    }
}