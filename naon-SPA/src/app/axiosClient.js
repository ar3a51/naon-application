import axios from 'axios';

var axiosInstance = axios.create({
   withCredentials: true
});
module.exports = axiosInstance;