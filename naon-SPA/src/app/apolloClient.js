import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { createUploadLink }  from 'apollo-upload-client';
import { WebSocketLink }       from 'apollo-link-ws';
import { InMemoryCache } from 'apollo-cache-inmemory'
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';

// HTTP connection to the API
const httpLink = createUploadLink({
  // You should use an absolute URL here
  uri: process.env.VUE_APP_NAON_ENDPOINT,
  credentials: "include"
});

const wsLink = new WebSocketLink({
  uri: process.env.VUE_APP_WEB_SOCKET,
  options: {
    reconnect: true,
  }
});

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
  },
  wsLink,
  httpLink
)

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
export const apolloClient = new ApolloClient({
  link,
  cache,
})