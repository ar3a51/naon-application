import store from './store/store';
import { isServerAuthenticated }   from './shared/services/authenticationService';

//const loginPage = () => import("./home/home");
const loginPage = () =>import("./login/components/loginPage");
const mainPage = () => import("./main/main.vue");
const registerPage = () => import("./registration/components/registrationPage");

const newsfeedComponent = () => import(/*webpackChunkName: newsfeed*/ "./main/newsfeed/newsfeed.vue");
const profileComponent = () => import(/*webpackChunkName: profile*/ "./main/profile/profile.vue");
const searchComponent = () => import("./main/search/search");
const postComponent = () => import("./main/post/post");
const accountSettingComponent = () => import("./main/account-settings/accountSetting")
const messageComponent = () => import("./main/message/message.vue");

const isAuthenticated = async (to, from, next) => {
  
    let result = await isServerAuthenticated();
    if(!result.data.whoAmI)
        next({name: "login"});
    else
        next();
}

const redirectToMain = async (to, from ,next) => {
    
    let result = await isServerAuthenticated();
    if(result.data.whoAmI)
        next({name:"mainPage"})
    else{
        next();
    }
}

export const routes = [
    {path: '/', component: loginPage, name: "login", beforeEnter: redirectToMain},
    {path: '/register', component: registerPage, name: "register"},
    {
        path: '/main', 
        component: mainPage,
        beforeEnter: isAuthenticated,
        children:[
            {path: '/', component: newsfeedComponent,  name: "mainPage"},
            {path: '/main/profile', component: profileComponent},
            {path: '/main/account-setting', component: accountSettingComponent},
            {path: '/main/message', component: messageComponent, props:(route)=>({username: encodeURIComponent(route.query.username)})},
            {path: '/main/search', component: searchComponent, props:(route)=>({q: encodeURIComponent(route.query.q)})},
            {path: '/main/post',component:postComponent, props: (route)=>({id: encodeURIComponent(route.query.id)})}
        ],
    },
];