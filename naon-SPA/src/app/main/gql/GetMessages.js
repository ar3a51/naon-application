 import gql from 'graphql-tag';
 
 export const getMessages = gql`
 query getMessages($pageNumber: Float!){
    messages(pageNumber: $pageNumber){
        total,
        totalUnread,
        items {
            from
            to
            isRead
            date
            message
            id
        }
    }
}`;