import axios from 'axios';

//get post
export default function getPost(id){
    return axios.get(`/post/${id}`).then(response => {
        return response.data 
    })
}