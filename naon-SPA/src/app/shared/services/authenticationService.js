import {apolloClient}   from '../../apolloClient';
import gql  from 'graphql-tag'
import axios from 'axios'

const IS_LOGIN_QUERY = gql` query isLogin
    {
        whoAmI
    }
`;

export async function isServerAuthenticated(){
    
   
    let result = await apolloClient.query({
        query: IS_LOGIN_QUERY,   
    })


    return result;

}