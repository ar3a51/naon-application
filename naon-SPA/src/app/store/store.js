import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {apolloClient} from '../apolloClient';
import gql from 'graphql-tag';

Vue.use(Vuex)

const AUTH_LOGIN = "AUTH_LOGIN";
const AUTH_REQUEST = "AUTH_REQUEST";
const AUTH_LOGOUT = "AUTH_LOGOUT";
const AUTH_SUCCESS = "AUTH_SUCCESS";
const AUTH_ERROR = "AUTH_ERROR";
const AUTH_REGISTER = "AUTH_REGISTER";

const USER_REQUEST = "USER_REQUEST";


export default new Vuex.Store({
  state: {
    token: "",
    status: "",
  },
  mutations: {
    [AUTH_REQUEST]: (state) => {
      state.status = 'Signing In...'
    },
    [AUTH_REGISTER]: (state) => {
      state.status = "Registering..."
    },
    [AUTH_SUCCESS]: (state, token) => {
      state.status = 'Success';
      state.status = "";
      state.token = token;
    },
    [AUTH_ERROR]: (state) => {
      state.status = 'Invalid Username or Password';
    },
    [AUTH_LOGOUT]:(state)=>{
      state.token = "";
      state.status = "Logged Out";
    }
  },
  getters: {
    getToken: (state) => {
      return state.token;
    },
  },
  actions: {
    [AUTH_REQUEST]: ({commit, dispatch}, user)=> {
      return new Promise(async (resolve, reject)=> {
        commit(AUTH_REQUEST);
       /* axios({url: "/signin", data: user, method: "POST"})
          .then(resp => {
          
            commit(AUTH_SUCCESS, "");
            resolve(resp);
          })
          .catch(err => {
            commit(AUTH_ERROR, err)
        
          })*/

          let result = await apolloClient.mutate({
            mutation: gql`mutation login ($username: String!, $password: String!){
              login(username: $username, password: $password)
            }`,
            variables: {
                username: user.username,
                password: user.password
            }
          });
          if (result.data.login){
              commit(AUTH_SUCCESS, "");
              resolve();
            }
          else if (!result.data.login){

            commit(AUTH_ERROR, "Invalid username");
            reject()
          } else if (result.errors && result.errors.length > 0) {
            commit(AUTH_ERROR, result.errors[0].message);
            reject();
          }
      })
    },
    [AUTH_REGISTER]:({commit, dispatch}, user)=> {
      return new Promise( async (resolve, reject)=> {
        commit(AUTH_REGISTER);
        axios({url: "/signin/register", data: user, method: "POST", headers: {"Content-Type": "application/x-www-form-urlencoded"}})
          .then(resp => {
            const token = resp.data.authorisedToken;
            localStorage.setItem("user-token", token);
            axios.defaults.headers.common["authorised-token"] = token;
            commit(AUTH_SUCCESS, token);
            resolve(resp);
          })
          .catch(err => {
            commit(AUTH_ERROR, err)
            localStorage.removeItem('user-token') // if the request fails, remove any possible user token if possible
            reject(err)
          })
      })
    },
    [AUTH_LOGOUT]: async ({commit, dispatch}) => {
     /* return new Promise( async (resolve, reject) => {
        axios({url: "/signout"})
        .then(resp => {
          commit(AUTH_LOGOUT, "");
          resolve(resp);
        })
        .catch(err => {
          commit(AUTH_ERROR, err)
         // if the request fails, remove any possible user token if possible
          reject(err)
        })
      })*/
      return new Promise(async (resolve, reject)=>{
          let result = await apolloClient.mutate({
            mutation: gql`mutation logout{
              logout
            }`,
          });
          if (result.data.logout){
              commit(AUTH_LOGOUT, "");
              resolve();
            }
          else{
            commit(AUTH_ERROR, result.errors[0].message);
            reject()
          }
      });
        
  }

    /*createToken: (context, payload) => {
      context.commit("storeToken",payload);
    }*/

  }
})
