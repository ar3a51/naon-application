const express = require('express');
const path = require('path');
const dotenv = require('dotenv');
dotenv.config();

const portNumber = process.env.VUE_APP_PORT || 3032;

const app = express();


app.use(express.static(path.resolve('dist')));

app.listen(portNumber,()=> {
    console.log(`Listening on port ${portNumber}`);
});
