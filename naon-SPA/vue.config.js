const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    outputDir: './dist',
    configureWebpack: {
        entry: [
            "./src/app/main.js"
        ],
        plugins:[
        new CopyWebpackPlugin(
            [
              {
                from: './public',
                to: './dist',
                ignore: [
                  '.DS_Store',
                  'server.js',
                  'index.html',
                  'cert',
                  'nginx.conf'
                ]
              }
            ]
          )
        ]
    }
}