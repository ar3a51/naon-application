const gulp = require('gulp');
const path = require('path');
const ts = require('gulp-typescript');
const clean = require('del');
const uglify = require('gulp-uglify-es').default;
const exec = require("child_process").exec;

const tsProject = ts.createProject('tsconfig.json');
const files = ['package.json', '.env'];
const dest = './dist/';

gulp.task('clean',function (){
    return clean(['dist/**/*', "./bundled/**/**"]);
})

gulp.task('build',function (cb) {

      return tsProject.src()
        .pipe(tsProject())
        .pipe(gulp.dest(dest));
        //.js.pipe(gulp.dest(dest))

   /* exec("npx webpack", function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
      });*/
})

gulp.task('copy dependency files',function() {
    return gulp
        .src(files)
        .pipe(gulp.dest(dest));

})

exports.default = gulp.series(gulp.task('clean'), gulp.task('build'));