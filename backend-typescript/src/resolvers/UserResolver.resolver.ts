import {
    Resolver,
    Mutation,
    Query,
    Arg,
    InputType,
    Field,
    ObjectType,
    FieldResolver,
    Root,
    Args,
    Authorized
}   from 'type-graphql'

import { UserTypes, UserDetailsType }   from '../types';

import { UserServiceCRUD }   from '../services/userServices/userServiceCRUD';
import { UserServiceQuery }   from '../services/userServices/userServiceQuery';
import { type } from 'os';
import { SignInService } from '../services/administrationService/signInService';


@ObjectType()
class AuthTokenType {
    @Field()
    authToken: string;
}


@Resolver(of => UserTypes)
export class UserResolver {
    private _userCrud: UserServiceCRUD;
    private _userQuery: UserServiceQuery;

    constructor(){
        this._userCrud = new UserServiceCRUD();
        this._userQuery = new UserServiceQuery();    
    }

    @Mutation(returns => AuthTokenType)
    async registerUser(
        @Arg("username") username:string,
        @Arg("password") password: string,
        @Arg("email") email: string
    ):Promise<AuthTokenType> {

        let existingUser = await this._userQuery.getUserByUsername(username);

        if (!existingUser) {
            let result = await  this._userCrud.registerUser({
                username: username,
                password: password,
                email: email
            });

           
            let signInService = new SignInService();
            let token: string = signInService.generateWebToken({},"");
            let authToken: AuthTokenType = {
                authToken: token
            }; 
            return authToken; 
        } else {
            throw new Error("user already exists");
        }

       
    }

    @Authorized()
    @Query(returns =>UserTypes)
     async user(
        @Arg("username") username: string
     ): Promise<UserTypes>{
       let result = await this._userQuery.getUserByUsername(username);

       let user:UserTypes = new UserTypes();
       user.username = result.username;
       user.email = result.email;

       return user;
    }

    @Authorized()
    @FieldResolver()
    async userDetails(@Root() user:UserTypes){
        let result = await this._userQuery.getUserDetailsByUsername(user.username);
        
        let userDetails: UserDetailsType = new UserDetailsType();
        userDetails.firstName = result.firstname;
        userDetails.lastName = result.lastname;
        userDetails.address1 = result.address1;
        userDetails.address2 = result.address2;
        userDetails.suburb = result.suburb;
        userDetails.postcode = result.postcode;

        return userDetails
    }
}