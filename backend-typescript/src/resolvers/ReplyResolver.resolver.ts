import { Resolver, Query, Arg, FieldResolver, Root, Args, Authorized } from "type-graphql";
import { ReplyType } from "../types";
import { ReplyServiceQuery } from "../services/replyServices/replyServiceQuery";


@Resolver(of => ReplyType)
export class ReplyResolver{
    private _replyQuery: ReplyServiceQuery;

    constructor(){
        this._replyQuery = new ReplyServiceQuery();
    }

    @Authorized()
    @Query(returns => ReplyType)
    async reply(
        @Arg("replyId") replyId:string
    ):Promise<ReplyType>{
        let reply = await this._replyQuery.GetReply(replyId);

        let replyType = new ReplyType()
        replyType.message = reply.message;
        replyType.id = reply.id;
        replyType.post_id = reply.post_id.toString();
        replyType.username = reply.username.toString();
        replyType.date = reply.date;
        replyType.modifiedDate = replyType.modifiedDate;
        
        return replyType;
    }

    @Authorized()
    @FieldResolver()
    async childReplies(
        @Root() reply:ReplyType,
        @Arg("pageNumber") pageNumber:number
    ):Promise<ReplyType[]>{
        let childReplies = await this._replyQuery.GetChildReplies(reply.id, pageNumber,"");

       return childReplies.map(r => {
            let replyType:ReplyType = new ReplyType();
            
            replyType.post_id = reply.post_id;
            replyType.message = r.message;
            replyType.modifiedDate = r.modifiedDate;
            replyType.date = r. date;
            replyType.parent_reply_id = reply.id;
            replyType.username = reply.username;

            return replyType;
        })
    }
}