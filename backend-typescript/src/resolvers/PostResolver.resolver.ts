import { Resolver, Query, Arg, FieldResolver, Root, PubSub, Mutation, Ctx, ObjectType, Field, Subscription, Publisher, Authorized } from "type-graphql";
import { PostType, LocationType, LikeType, ImageType, ReplyType } from "../types";
import { PostServiceCrud } from "../services/postServices/postCRUD";
import { PostQuery, IResult } from "../services/postServices/postQuery";
import { Request } from "express";
import PaginatedResponse from "../types/pagingType";
import { ReplyServiceQuery }   from '../services/replyServices/replyServiceQuery';
import { ReplyServiceCRUD }   from '../services/replyServices/replyServiceCRUD';
import { IReplyDomain } from "../models/replies";
import { IPostDomainModel } from "../models/posts";
import { GraphQLUpload }       from 'graphql-upload';
import { Upload } from "../types/upload";
import { Stream, Readable } from "stream";
import { createWriteStream } from "fs";
import { resolve } from "dns";
import { exceptions } from "winston";
import { TestType } from "../types/testUploadType";
import * as winston from 'winston';


@ObjectType()
class PostResponse extends PaginatedResponse(PostType){

}

interface IAttachment {
    filename: string,
    fileBuff: Buffer,
    mimeType: string,
}


@Resolver(of => PostType)
export class PostResolver  {

    private _postCrud:PostServiceCrud;
    private _postQuery:PostQuery
    private _replyQuery: ReplyServiceQuery;
    private _replyCrud: ReplyServiceCRUD;

    constructor(){
      
        this._postCrud = new PostServiceCrud();
        this._postQuery = new PostQuery();
        this._replyQuery = new ReplyServiceQuery();
        this._replyCrud = new ReplyServiceCRUD();
        
    }

    @Subscription({
        topics: ["NEW_POST"],
        filter:({payload, args, context:{connection: {context:{req}}}})=> {
            return payload.username.toLowerCase() == req.session.userId},
      })
    newPost(
        @Root() postPayload: PostType,
        @Arg("username") username: string
    ): PostType {
        return postPayload;
    }

    @Authorized()
    @Query(returns => PostResponse)
    async posts(
        @Arg("pageNumber") pageNumber: number,
        @Ctx() ctx:any
    ):Promise<PostResponse>{
        let totalResultCount = 0;
        let results = await this._postQuery.getPost(
            pageNumber, 
            ctx.req.session!.userId
            );

        let resultToSend = await Promise.all(results.result.map(r => {
            return this.mapData(r,<Request>ctx.req)})
        );

        return {
            items: resultToSend,
            total: results.totalRecords
        }

    }

    @Mutation(returns => Boolean)
    async testUpload(
        @Arg("message", type => String, {nullable: true}) message: String,
        @Arg("files",type => [GraphQLUpload], {nullable: true})files:[Upload]
        /*@Arg("test", type => TestType) test: TestType*/):Promise<Boolean>{
           
            
                winston.info("Inside test upload",);
                let attachments:IAttachment[] = [];
               
                console.log(files[0])
                if (files){
                   
                   
                    let result = await Promise.all(files.map(file => {
                        return this.extractAttachment(file);
                    }))

                    console.log(result[1].fileBuff.toString());
                   
                }

               

              
               /* let buffer = await new Promise<Buffer>(async (resolve, reject)=>{
                    let buffers = [];
                    a.createReadStream().on("data",(data)=>{
                            
                            buffers.push(data)
                            console.log("pusshing data")
                        }).on("end", async ()=>{
                            finalBuffers = Buffer.concat(buffers);
                            console.log("finish data")
                            resolve(finalBuffers);
                        }).on("error",(err)=>{
                            console.error("error");
                        });
        
                });*/
                

                //console.log(attachments);

           /*    let buffer = await new Promise<Buffer[]>((resolve, reject)=>{
                    let buffers = [];
                    createReadStream().on("data",(data)=>{
                        buffers.push(data)
                        resolve(buffers);
                    }).on("end",()=>{
                        resolve(null);
                    });
               })

               if (buffer)
                finalBuffers = Buffer.concat(buffer);

                console.log(finalBuffers);*/
               
                return true;
          
    }

    @Authorized()
    @Mutation(returns => Boolean)
    async postMessage(
        @Arg("message") message: string,
        @Arg("subject") subject: string,
        @Arg("postType") postType: string,
        @Arg("latitude", {nullable: true}) latitude: string,
        @Arg("longitude", {nullable: true}) longitude: string,
        @Arg("files",type => [GraphQLUpload], {nullable: true})files:Upload[],
        @PubSub("NEW_POST") publish:Publisher<PostType>,
        @Ctx() ctx:any
    ):Promise<PostType>{
        
       
        try{
            /*winston.log("info",`PostResolvers: Receiving following info ${JSON.stringify({
                message,
                username,
                subject,
                postType,
                latitude,
                longitude,
                files: (files)?await Promise.all(files.map(async f => (await f).filename )):[],
            })} at ${new Date()}`);*/

           winston.info("Receiving the following information", {
            message,
            username: ctx.req.session!.userId,
            subject,
            postType,
            latitude,
            longitude,
            files: (files)?await Promise.all(files.map(async f => (await f).filename )):[],
        });

            let attachments: IAttachment[] = [];

            if (files){
                    
                    
            attachments = await Promise.all(files.map(file => {
                    return this.extractAttachment(file);
                }))

            
            }
        

            let postModel:IPostDomainModel = {
                username: ctx.req.session!.userId,
                message: message,
                subject,
                postType,
                location: {
                    latitude,
                    longitude
                },
                attachments: attachments.map(a => {
                    return {
                    filename: a.filename,
                    file: a.fileBuff,
                    mimeType: a.mimeType
                    }
                })
            }

            
            let result = await this._postCrud.insertPost(postModel);
           
            let postTypeResult:PostType = new PostType();
            postTypeResult.id = result._id.toString();
            postTypeResult.location = postModel.location;
            postTypeResult.message = postModel.message;
            postTypeResult.postDate = result.postedDate;
            postTypeResult.username = result.username;
            postTypeResult.postType = postModel.postType;
            
        // publish(postTypeResult);*/
            
            return(postTypeResult);
        }catch(err){
            winston.error("postMessage Error: ",err);
            return null;
        }
    }

    @Authorized()
    @Mutation(returns => ReplyType)
    async postComment(
        @Arg("postId") postId: string,
        @Arg("parent_reply_id", {nullable: true}) parentReplyId: string,
        @Arg("reply_to_id" ,{nullable: true}) replyToId:string,
        @Arg("message") message: string,
        @Ctx() ctx: any
    ):Promise<ReplyType>{
        let reply:IReplyDomain = { 
            message:message, 
            username: ctx.req.session!.userId, 
            post_id: postId,
            parent_reply_id: parentReplyId,
            reply_id:"",
            reply_to_id: replyToId
        };
        let result = await this._replyCrud.addReply(reply)

        let replyType: ReplyType = new ReplyType();
        replyType.id = result.id;
        replyType.message = result.message;
        replyType.date = result.modifiedDate;
        replyType.parent_reply_id = result.parent_reply_id ? result.parent_reply_id.toString():null;
        replyType.post_id = result.post_id.toString();
        replyType.username = result.username.toString();


        return replyType
       
    }

    @FieldResolver()
    async likeCount(@Root() post:PostType):Promise<number>{
        return await this._postQuery.getLikeCount(post.id);
    }

    @FieldResolver()
    async likes(@Root() post:PostType, @Ctx() context:any): Promise<LikeType[]> {
      
        let result = await context.likeLoader.load(post.id);
        context.likeLoader.clear(post.id);
        let resToSend: LikeType[] = result.map(r => {

            let like = new LikeType()
            like.username = r.username;
            like.postId = post.id;
            like.replyId = "";
            

            return like;
        });

        return resToSend;
      
    }

    @Authorized()
    @FieldResolver()
    async replies(@Root() post:PostType, @Ctx() context:any):Promise<ReplyType[]>{
      
        let result = await context.replyLoader.load(post.id);
        context.replyLoader.clear(post.id);

        return result.map(r => {
            let replyType: ReplyType = {
                message: r.message,
                username: r.username,
                id: r.id,
                date: r.date,
                modifiedDate: r.modifiedDate,
                parent_reply_id: "",
                post_id: post.id,
                childReplies: []
            }

            return replyType
        });
      
    }

    private mapData(data:IResult, req:Request): PostType {

        let postType = new PostType();

        postType.username = data.username;
        postType.id = data._id.toString();
        postType.message = data.description;
        postType.postType = data.postType;
        postType.postDate = data.dateTime;
        postType.subject = data.subject;

        if (data.location){
            postType.location = new LocationType()
            postType.location.latitude = data.location.latitude;
            postType.location.longitude = data.location.longitude;
        }

        /*postType.imageUrl =  (data. && data.image.length > 0) ? 
        `${req.protocol}://${req.get('host')}/image/${data._id}` : null*/

        postType.attachments = data.attachments.map(a => {
            return `${req.protocol}://${req.get('host')}/post/${postType.id}/attachment/${a}`;
        })
    
        return postType;
    }

    private async extractAttachment(file: Upload): Promise<IAttachment>{

        let finalBuffers:Buffer = null;
               
        let fileObj = await file;
      
        await new Promise<Buffer>(async (resolve, reject)=>{
            try {
                let buffers = [];
                fileObj.createReadStream().on("data",(data)=>{
                    
                    buffers.push(data)
                   
                }).on("end", async ()=>{
                    finalBuffers = Buffer.concat(buffers);
                    resolve(finalBuffers);
                });
            } catch(err){
                reject(err);
            }

        }).catch(err =>{
           
            winston.error("Error occurred",err)
        });

        return {
            filename: fileObj.filename,
            fileBuff: finalBuffers,
            mimeType: fileObj.mimetype,
        }
    }


}

