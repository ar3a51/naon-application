import { Resolver, Mutation, Args, Arg, Ctx, Query, Authorized } from "type-graphql";
import { SignInService }   from '../services/administrationService/signInService';
import { Context }           from '../context/context';
import { GraphQLScalarType } from "graphql";



@Resolver()
export class LoginResolver {
   
    private _signInService:SignInService;
    constructor(){
        this._signInService = new SignInService();
    }
    @Mutation(() => Boolean)
    async login(
        @Arg("username") username: string,
        @Arg("password") password: string,
        @Ctx() context:any
    ): Promise<boolean>{
       let signinResult = await this._signInService.verifyUser(username, password);
        
       if (!signinResult.result){
           return false;
       }

        context.req.session!.userId = signinResult.userObject.username;

       return true;

    }

    @Authorized()
    @Mutation(() => Boolean)
    async logout(
        @Ctx() context:any
    ): Promise<boolean>{

        return new Promise((res, rej)=>{
            context.req.session!.destroy(err => {
                if (err){
                    rej(err);
                }

                context.res.clearCookie("qid");
                res(true);
            });
        })    

    }

    @Query(returns => String, {nullable: true})
    async whoAmI(@Ctx() ctx:any):Promise<string>{
        if (!ctx.req.session!.userId){
            return null
        } else {
            return ctx.req.session!.userId
        }
    }
}