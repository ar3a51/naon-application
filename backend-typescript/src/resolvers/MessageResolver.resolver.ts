import { Resolver, 
        Query, 
        Arg, 
        FieldResolver, 
        Root, 
        Mutation, 
        Ctx, 
        ObjectType, 
        Subscription, 
        Args, 
        PubSub, 
        Publisher, 
        Field, 
        Authorized } from "type-graphql";
import {MessageCrud}   from '../services/messageServices/messageCrud'
import {MessageQuery}   from '../services/messageServices/messageQuery';
import { MessageType } from "../types/messageType";
import PaginatedResponse from "../types/pagingType";
import { IMessageDomain } from "../models/messages";

@ObjectType()
class MessageResponse extends PaginatedResponse(MessageType){
    @Field()
    public totalUnread: number;
}

@Resolver()
export class MessageResolver {
    private _messageQuery:MessageQuery;
    private _messageCrud:MessageCrud;
    constructor() {
        this._messageQuery = new MessageQuery();
        this._messageCrud = new MessageCrud();
    }

    @Subscription({
        topics: ["NEW_MESSAGE", "UPDATED_MESSAGE"],
        filter:({payload, args, context:{connection: {context:{req}}}})=> {
            return payload.to.toLowerCase() == req.session.userId},
      })
    newMessage(
        @Root() messagePayload: MessageType,
    ): MessageType {
        return messagePayload;
    }

    @Authorized()
    @Query(returns => MessageResponse)
    async messages(
        @Arg("pageNumber") pageNumber:number,
        @Ctx() ctx:any
    ):Promise<MessageResponse>{
      
        let result = await this._messageQuery.getMessages(ctx.req.session!.userId, pageNumber);

        let resultToReturn = result.messages.map((r:any) => {
           
            let messageType = new MessageType();

            messageType.date = r.dateSent;
            messageType.from = r.from;
            messageType.message = r.message;
            messageType.to = r.to;
            messageType.isRead = r.isRead;
            messageType.id = r.id.toString();
            return messageType;
        });

        let messageResp:MessageResponse = new MessageResponse();
        messageResp.items = resultToReturn;
        messageResp.total = result.totalRecordCount;
        messageResp.totalUnread = result.totalUnread;

        return messageResp;
    }

    @Authorized()
    @Query(returns => MessageResponse)
    async conversations(
        @Arg("from") from: string,
        @Arg("pageNumber") pageNumber: number,
        @Arg("username") username: string
    ):Promise<MessageResponse>{
        let result = await this._messageQuery.getConversations(from, pageNumber, username);

        let resultToReturn = result.messages.map((r:any) => {
           
            let messageType = new MessageType();

            messageType.date = r.dateSent;
            messageType.from = r.from;
            messageType.message = r.message;
            messageType.to = r.to;
            messageType.isRead = r.isRead;
            messageType.id = r.id.toString();
            return messageType;
        });

        let messageResp:MessageResponse = new MessageResponse();
        messageResp.items = resultToReturn;
        messageResp.total = result.totalRecordCount;
        messageResp.totalUnread = result.totalUnread;

        return messageResp;
    }

    @Authorized()
    @Mutation(returns => MessageType)
    async markAsRead(
        @Arg("messageId") messageId:string,
        @PubSub("UPDATED_MESSAGE") publish: Publisher<MessageType>
    ):Promise<MessageType>{
       let message = await this._messageCrud.markAsRead(messageId);

       let messageType = new MessageType()
       messageType.date = message.date;
       messageType.from = message.from;
       messageType.isRead = true;
       messageType.message = message.message;
       messageType.id = message._id.toString();
       messageType.to = message.to;

       await publish(messageType);
       
       return messageType;
    }

    @Mutation(returns => [MessageType])
    async markAllAsRead(
        @Arg("username") username: string,
    ):Promise<MessageType[]>{
       let result = await this._messageCrud.markAllAsRead(username);

       let resultToReturn = result.map((r:any) => {
           
            let messageType = new MessageType();

            messageType.date = r.dateSent;
            messageType.from = r.from;
            messageType.message = r.message;
            messageType.to = r.to;
            messageType.isRead = r.isRead;
            messageType.id = r.id.toString();
            return messageType;
        });

        return resultToReturn;
    }

    @Mutation(returns => MessageType)
    async sendMessage(
        @Arg("message") msg: string,
        @Arg("from") fromUsername: string,
        @Arg("to") toUsername: string,
        @PubSub("NEW_MESSAGE") publish: Publisher<MessageType>
    ):Promise<MessageType>{

        let messageDomain:IMessageDomain = {
          message: msg,
          from: fromUsername,
          to: toUsername,
          
        }
        let message = await this._messageCrud.insertMessage(messageDomain);

        let messageType = new MessageType()
        messageType.date = message.date;
        messageType.from = message.from;
        messageType.isRead = false;
        messageType.message = message.message;
        messageType.id = message._id.toString();
        messageType.to = message.to;

        await publish(messageType);
        
        return messageType;
    }
}