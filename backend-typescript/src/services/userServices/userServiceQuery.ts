import { UserModel, 
         IUser }          from '../../models/users';
import { UserDetailsModel, 
         IUserDetails   }     from '../../models/userDetails';
import * as bcryptjs from 'bcryptjs';

export class UserServiceQuery {
    constructor (){}

   public async getUserByUsername(username:string):Promise<IUser> {
    
         return  await UserModel.findOne({"username": username});
       
    }

   public async getUserDetailsByUsername(username:string):Promise<IUserDetails> {
      return await UserDetailsModel.findOne({"username":username});
   }

   public async verifyUser(username:string, password: string):Promise<boolean> {
      let user:IUser = await this.getUserByUsername(username);

      return await bcryptjs.compare(user.password, password);
   }

}