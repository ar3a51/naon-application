import { UserModel, IUser } from '../../models/users';
import { UserDetailsModel } from '../../models/userDetails';
import * as bcryptjs from 'bcryptjs';
import { UserServiceQuery }   from './userServiceQuery';

import {IUserDetails}   from '../../models/userDetails';

export class UserServiceCRUD {
    constructor(){}

    public async registerUser(user:{username: string, password: string, email:string}) {

        let salt = await bcryptjs.genSalt(parseInt(process.env.BCRYPT_SEED_HASH));
        let userModel:IUser = new UserModel({
            username: user.username,
            password: await bcryptjs.hash(user.password, salt),
            email: user.email,
        });

       let userModelResult:IUser = await userModel.save();

       return userModelResult;
    }

    public async registerUserProfile(username: string, profileModel:any)
    {
        let userDetailsModel:IUserDetails = new UserDetailsModel({
            username: username,
            firstname: profileModel.firstname,
            middlename: profileModel.middlename,
            lastname: profileModel.lastname,
            photoprofile: profileModel.photoprofile,
            address1: profileModel.address1,
            address2: profileModel.address2,
            suburb: profileModel.suburb,
            postcode: profileModel.postcode,
        });

        let userDetailsModelResult:IUserDetails = await userDetailsModel.save();

        return userDetailsModel;
    }

    async updatePassword(user){

        try {
            let userQuery = new UserServiceQuery();
            let userObj = await userQuery.getUserByUsername(user.username);
            
            userObj.set({password: user.password});
            await user.save();
        } catch(ex) {
            throw ex;
        }

    }

}