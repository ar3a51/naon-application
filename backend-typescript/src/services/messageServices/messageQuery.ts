import { MessageModel, IMessage }       from '../../models/messages';
import { UserServiceQuery }       from '../userServices/userServiceQuery';

export class MessageQuery {

    private _userQuery: UserServiceQuery = null;

    constructor(){
        this._userQuery = new UserServiceQuery();
    }

    public async getMessages(username: string, pageNumber: number){
        
        let limit:number = 5;
        let totalRecords = await MessageModel.find({to: username}).estimatedDocumentCount();
        let messages = await MessageModel.find({to: username}).sort({date: -1}).limit(limit).skip((pageNumber - 1) * limit);
       
        return {
            messages: messages.map(m => this.mapToResult(m)),
            totalRecordCount: totalRecords,
            totalUnread: messages.filter(m => m.isRead == false).length
        }
    }

    public async getConversations(from: string, pageNumber: number, userName: string){
        let limit:number = 5;
        let totalRecords = await MessageModel.find({from: {"$in": [from, userName]}, to: {"$in": [from, userName]}}).estimatedDocumentCount();
        let messages = await MessageModel.find({from: {"$in": [from, userName]}, to: {"$in": [from, userName]}}).sort({date: 1}).limit(limit).skip((pageNumber - 1) * limit);
       
        return {
            messages: messages.map(m => this.mapToResult(m)),
            totalRecordCount: totalRecords,
            totalUnread: messages.filter(m => m.isRead == false).length
        }
    }

    private mapToResult(message: IMessage) {
      
        return {
            message: message.message,
            from: message.from,
            dateSent: message.date,
            id: message._id,
            isRead: message.isRead,
            to: message.to,
        }
    }
}