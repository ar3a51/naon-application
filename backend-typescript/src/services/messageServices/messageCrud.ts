import { MessageModel, 
         IMessage,
         IMessageDomain }       from '../../models/messages';

export class MessageCrud {
    constructor() {}

    public async insertMessage(message:IMessageDomain){

       let messageModel:IMessage = new MessageModel({
           from: message.from,
           to: message.to,
           message: message.message
       });

      return await messageModel.save();
    }

    public async markAsRead(id: string): Promise<IMessage> {
       return  await MessageModel.findByIdAndUpdate(id, {isRead: true});
    }

    public async markAllAsRead(username: string): Promise<IMessage[]> {
       let messages:IMessage[] = await MessageModel.find({to: username, isRead: false});

        await MessageModel.updateMany(
           {to: username, isRead: false},
           { $set:{isRead: true}}
         );

         let messageIds:string[] = messages.map((m)=> m._id.toString());

         let result = await MessageModel.find({_id: {$in: [messageIds]}});

         return result;
    }


}