import { ReplyModel, IReplyDomain, IReply }       from '../../models/replies';
import { UserServiceQuery }                   from '../userServices/userServiceQuery';
import mongoose from 'mongoose';
import { ObjectId, BSONType } from 'mongodb';
import { stringify } from 'querystring';
const DataLoader = require("dataloader");


export class ReplyServiceQuery {

    _userQuery = null;
    constructor() {
        this._userQuery = new UserServiceQuery();
    }
    
    async GetReply(replyId){
        
        let reply = await ReplyModel.findById(replyId);
        let user = await this._userQuery.getUserDetailsByUsername(reply.username);

        return this.MapToReturnObject(reply, user);
    }

    async GetReplyByPostId(postIds: string[], pageNumber: number, username:string) {
      
        let limit = 5;
        let response = [];
        let replies = await ReplyModel.find({
            "post_id":{$in: postIds}
        }).sort({"modifiedDate": -1}).limit(limit).skip((pageNumber - 1) * limit);
       
        response = replies.map(r => {return this.MapToReturnObject(r, null)});
       
       return response;
    }

    async GetChildReplies(parentCommentId: string, pageNumber: number, username: string){

        let limit = 5;
        let response = [];

        let user = await this._userQuery.getUserDetailsByUsername(username);

        let comments = await ReplyModel.find({
            "parent_reply_id": mongoose.Types.ObjectId(parentCommentId),
        }).limit(limit).skip((pageNumber - 1) * limit);

        response = await Promise.all(comments.map(c => {return this.MapToReturnObject(c, user)}));
             
        return response;
    }

    getReplyLoader(pageNumber:number = 1 ){
        return new DataLoader(async postIds => {
            let result = await this.GetReplyByPostId(postIds,pageNumber,"")
            return postIds.map(k => result.filter(r => r.post_id == k));
        });
    }

    MapToReturnObject(reply:IReply, user){
       
        return {
            post_id: reply.post_id,
            id: reply._id.toString(),
            message: reply.message,
            username: reply.username,
            date: reply.date,
            modifiedDate: reply.modifiedDate,
        }
    }
}