import { ReplyModel, IReplyDomain, IReply }       from '../../models/replies';
import { ReplyServiceQuery }                   from './replyServiceQuery';
import mongoose from 'mongoose';


export class ReplyServiceCRUD {

    _replyServiceQuery = null;
    constructor(){
        this._replyServiceQuery = new ReplyServiceQuery();
    }

    async addReply(reply:IReplyDomain):Promise<IReply>{
      
        let replyModel = new ReplyModel({
            username: reply.username,
            post_id: mongoose.Types.ObjectId(reply.post_id),
            parent_reply_id: reply.reply_to_id ? mongoose.Types.ObjectId(reply.reply_to_id):null,
            message: reply.message,
       });

       return await replyModel.save();
    }

    async editReply(reply){
      let currReply = await  this._replyServiceQuery.GetReply(reply.id);

      currReply.set({
          message: reply.message,
          modifiedDate: new Date(),
      });
    }
}