import { PostModel }   from '../../models/posts';

export class ImageQuery {
    public async GetImage(postId: string):Promise<{file: Buffer, mimeType: string, filename: string}>{
        let image = (await PostModel.findById(postId).select('image -_id')).attachments[0];
        return image;
    }
}