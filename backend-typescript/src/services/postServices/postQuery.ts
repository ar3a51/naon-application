import { PostModel, IPost }                   from '../../models/posts';
import { LikeModel, ILike }                   from '../../models/likes';

import { UserServiceQuery }       from '../userServices/userServiceQuery';
import { PostSearchApiService }                        from '../searchApiServices/postSearchServices';
const DataLoader = require("dataloader");


export interface IResult {
    _id: string,
    username: string,
    photoProfile: Buffer,
    subject: string,
    postType: string,
    dateTime: Date,
    likes: any,
    attachments: string[],
    description: string,
    location: {
        latitude: string,
        longitude: string,
    }
}

export class PostQuery {

    private userQuery:UserServiceQuery = null;
    private _postSearch:PostSearchApiService = null;

    constructor() {
        this.userQuery = new UserServiceQuery();
        this._postSearch = new PostSearchApiService();
    }

    public async getPost(pageNumber: number, username: string) {
        
        let limit = 5;
        let results = [];
        let totalRecNumber = await PostModel.find({username}).countDocuments();
        
        let posts = await PostModel.find({username}).sort({dateTime: 'desc'}).limit(limit).skip((pageNumber - 1) * limit);
        
        let result = await Promise.all(posts.map(async (p)=> {
            return await this.mapToResult(p);
        }));
       
       return {
            totalRecords: totalRecNumber, 
            result
        }

    }

    public async getPostById(postId:string) {
        try {
            let post = await PostModel.findById(postId);
            return await this.mapToResult(post);
        } catch(ex){
            throw ex;
        }
    }

    public async searchPost(searchTerm:string){
        let searchResult = await this._postSearch.searchIndex(searchTerm);
        let postIds:string[] = searchResult.map(v => {return v.id});

        let posts = await PostModel.find({_id: {$in: postIds}});
        
        let result = await Promise.all(posts.map(async (p)=> {
            return await this.mapToResult(p);
        }));
        

       return result;

    }

    public async getLikes(postIds: string[]): Promise<any[]>{
        return await LikeModel.find({"_id": {"$in": postIds}});

    }

    public async getLikeCount(postId: string):Promise<number>{
        return await LikeModel.find({postId}).estimatedDocumentCount();
    }

    public async getLikers(postIds: [string]): Promise<{
        postId: string,
        username: string,
     }[]> {
        let likes = await LikeModel.find({"postId":{"$in": postIds}})
      
        return await Promise.all(likes.map(async like => {
            let user = await this.userQuery.getUserDetailsByUsername(like.username);

            return {
               postId: like.postId,
               /*photoProfile: user.photoProfile,*/
               username: like.username,
            }
        }))

        
    } 

    public getLikerLoader(){
        let loader = new DataLoader(async postIds => { 
            let result = await this.getLikers(postIds);

            return postIds.map((id:string) => result.filter(r => r.postId == id));
        });
        return loader;
    }

    private async mapToResult(post:IPost):Promise<IResult> {

        let user = await this.userQuery.getUserDetailsByUsername(post.username);
        let likes = await this.getLikers(post.id);

        let result:IResult = {
            _id: post.id,
            username : post.username,
            photoProfile: user.photoProfile,
            description: post.message,
            subject: post.subject,
            dateTime: post.dateTime,
            postType: post.postType,
            location: {
                latitude: post.location.latitude,
                longitude: post.location.longitude
            },
            likes: {
                count: likes.length,
                likers: likes
            },
            attachments: post.attachments.map(a => a.filename)
        }

        return result;
    }
}