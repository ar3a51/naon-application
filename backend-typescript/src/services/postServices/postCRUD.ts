import { 
        PostModel, 
        IPost, 
        IPostDomainModel 
    }       from '../../models/posts';
import { LikeModel }       from '../../models/likes';
import * as postApi   from '../searchApiServices/postSearchServices';
import axios from "axios";
import * as winston from 'winston'

export class PostServiceCrud {

    private _postSearchService: postApi.PostSearchApiService;

    constructor(){
        this._postSearchService = new postApi.PostSearchApiService();
    }

    async insertPost(post:IPostDomainModel)
    {
       /* let image: { imageFile: Buffer, description: string, extension:string} = null;
        let cognitiveApi: string = process.env.COGNITIVE_API;
        let tags:string[] = [];
        let imageDescription: string = "";
        let postedImage:any;

        if (post.image && post.image.imageFile && post.image.imageFile.length > 0)
            image = {
                imageFile: post.image.imageFile, 
                description: "",
                extension: post.image.extension
            };
        */
        try {
            let postModel = new PostModel({
                username: post.username,
                message: post.message,
                subject: post.subject,
                location: post.location,
                attachments: post.attachments,
                postType: post.postType,
            });

            winston.info(`Saving following object`,
                {
                    username: postModel.username,
                    message: postModel.message,
                    subject: postModel.subject,
                    location: postModel.location,
                    attachments: post.attachments.map(a => a.filename),
                    postType: post.postType,
                }
            );

            let postResult:IPost = await postModel.save();
            
            winston.log("info",`Saving successful, post_Id: ${postResult._id.toString()}`);
            
            /*if (postResult.image && postResult.image.imageFile && postResult.image.imageFile.length > 0) {
                postedImage = await axios.get(`${cognitiveApi}/api/${postResult.id}`).then(response => {return response.data;});
                imageDescription = postedImage.image.description;
                tags = postedImage.image.tags;
            }
            
          this._postSearchService.InsertIntoIndex({
                id: postResult.id,
                message: postResult.message,
                tags: tags,
                imageDescription: imageDescription,
                username: postResult.username
            }).then(resp => {console.log("post indexed")})
            .catch((err:any) =>{console.log(err.response.data)});*/

            return {
                _id: postResult.id,
                message: postResult.message,
                username: postResult.username,
                postedDate: postResult.dateTime,
                postType: postResult.postType,
                attachments:postResult.attachments.map(p => {return p.filename})
            };
            
        } catch (err){
            throw err;
        }

    }

    async updatePost(post: {id: any, message: string}) {
        let currPost = await PostModel.findById(post.id);
        currPost.set({message: post.message});
        
        return await currPost.save();
    }

    async like(postId: string, username: string){
        console.log(postId);
        console.log(username);
        let like = new LikeModel({
            postId,
            username
        });
       
        return await like.save();
    }
}