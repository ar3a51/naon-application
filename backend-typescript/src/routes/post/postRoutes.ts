import * as express from 'express';

import { asyncErrHandler } from "../../middlewares/asyncErrorHandler";
import { UserModel }   from '../../models/users';
import { IPostDomainModel }   from '../../models/posts';

import {PostServiceCrud}              from '../../services/postServices/postCRUD';
import {PostQuery}                    from '../../services/postServices/postQuery';

export class PostRoute {
    constructor(
        private _postCrud:PostServiceCrud,
        private _query:PostQuery,
        private _route:express.Router
    ){
        this.setupRoutes();
    }

    public getRoute():express.Router {
        return this._route;
    }

    private setupRoutes():void {

        this._route.get("/:id", asyncErrHandler(async(req:express.Request,res:express.Response)=>{
           
            let postId = req.params.id;
            
            let result = await this._query.getPostById(postId); //PostModel.findById(req.params.id);
         
            if (result)
             res.send(this.mapData(result,req));
            else
             res.sendStatus(404);
         }));

    /*     this._route.get("/getpost/:pagenumber", asyncErrHandler(async(req: express.Request, res:express.Response)=> {
            
            let pagenumber = req.params.pagenumber;
            let totalRec:number = 0;
            let results = await this._query.getPost(pagenumber,res.locals.userObj.username);
            let resultToSend = results.result.map(r => {
                return this.mapData(r,req)});
            res.send(resultToSend);

         }))*/
         
        /* this._route.post("/",asyncErrHandler(async(req:express.Request,res:express.Response)=>{
          
          let message = req.body.message;
          let postType = req.body.postType;
          let location: {latitude: string, longitude: string} = null;
          let image:{imageFile: Buffer, description: string, extension: string} = null;
          
          if (req.body.image && req.body.image.imageFile && req.body.image.imageFile.length > 0)
            image = { 
                      imageFile: Buffer.from(req.body.image.imageFile,'base64'), 
                      description: "",
                      extension: req.body.image.extension
                    };
        
          if (req.body.geolocation)
             location = {
                 latitude: (req.body.geolocation)? req.body.geolocation.latitude: '0',
                 longitude: (req.body.geolocation)? req.body.geolocation.longitude: '0',
             }

         

          let result =  await this._postCrud.insertPost({
              username: res.locals.userObj.username, 
              message,
              postType,
              location,
              image
            })

            res.send(this.mapData({
                _id: result._id,
                username: result.username,
                message: result.message,
                postType: result.postType,
                description: (result.image && result.image.description && result.image.description.length > 0 ) ? result.image.description : null,
                postedDate: result.postedDate,
                location,
                image:(result.image && result.image.imageFile && result.image.imageFile.length > 0)? result.image.imageFile: "", }, req));
         }));*/

         this._route.post("/like/:postid",asyncErrHandler(async(req:express.Request, res:express.Response)=>{
             let result = await this._postCrud.like(req.params.postid, res.locals.userObj.username);
             res.send(result);
         }))

         this._route.post("/search/",asyncErrHandler(async(req:express.Request, res:express.Response)=>{
             let results = await this._query.searchPost(req.body.searchTerm);

             let resultToSend = results.map(r => {
                return this.mapData(r,req)});

             res.send(resultToSend);
         }))

    }

    private mapData(data:any, req: express.Request): {id: string, 
                            username: string, 
                            message: string, 
                            postType: string,
                            description: string,
                            postedDate: Date,
                            likes: {count: number, likers: Array<any>}
                            image: string,
                            geolocation: {latitude: string, longitude: string} 
                            } {

        return {
            id: data._id,
            username: data.username,
            message: data.message,
            postType: data.postType,
            description: (data.description && data.description.length > 0) ? data.description : null,
            postedDate: data.postedDate,
            geolocation: {
                latitude: (data.location)? data.location.latitude:"0",
                longitude: (data.location)? data.location.longitude: '0',
            },
            likes: {
                count: (data.likes && data.likes.likers) ? data.likes.likers.length: 0,
                likers: (data.likes && data.likes.likers) ? data.likes.likers : []
            },
            image: (data.image && data.image.length > 0) ? 
            `${req.protocol}://${req.get('host')}/image/${data._id}` : null,
        }

    }
}



