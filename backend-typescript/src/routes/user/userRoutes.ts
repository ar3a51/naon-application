import * as express from 'express';
import { asyncErrHandler }   from    '../../middlewares/asyncErrorHandler';
import {Request, Response} from 'express';


import { UserServiceQuery }     from '../../services/userServices/userServiceQuery';
import { UserServiceCRUD}       from '../../services/userServices/userServiceCRUD';
import { json } from 'body-parser';


export class UserRoute {
    constructor(
        private _userQuery: UserServiceQuery,
        private _userCrud: UserServiceCRUD,
        private _route: express.Router
    ){
        this.setupRoute();
    }

    private setupRoute():void {

        this._route.get("/", asyncErrHandler(async(req:Request, res:Response) => {

         
            let userService = new UserServiceQuery();
            let result = await userService.getUserByUsername(res.locals.userObj.username)
        
            if (result)
                res.send(result);
            else
                res.status(404).send("not found");
          
        }));
        
        this._route.get("/userdetails", asyncErrHandler(async(req:Request,res:Response) => {
        
         
                let userQuery = new UserServiceQuery();
        
                let result = await userQuery.getUserDetailsByUsername(res.locals.userObj.username);
        
                if (result)
                    res.json(result);
                else
                    res.status(404).send("User not found");
           
        
        }));

        this._route.post("/registerProfile", asyncErrHandler(async(req:Request, res:Response)=> {

            let userCrud:UserServiceCRUD = new UserServiceCRUD();
            let userQuery = new UserServiceQuery();

            let result = await userQuery.getUserDetailsByUsername(res.locals.userObj.username);
        
            if (!result)
                res.status(404).send("User not found");
       
          let userDetails = await  userCrud.registerUserProfile(req.body.username,{
                firstname: req.body.firstname,
                middlename: req.body.middlename,
                lastname: req.body.lastname,
                photoprofile: req.body.photoprofile,
                address1: req.body.address1,
                address2: req.body.address2,
                suburb: req.body.suburb,
                postcode: req.body.postcode,
            })

            res.json(userDetails);

        }))

        
    }

    public getRoute():express.Router {
        return this._route;
    }
}


