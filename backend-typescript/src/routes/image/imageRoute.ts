import * as express from 'express';
import { asyncErrHandler } from "../../middlewares/asyncErrorHandler";
import { ImageQuery }   from '../../services/imageServices/imageQuery';

export class ImageRoute {
    constructor(
        private _imageQueryService: ImageQuery,
        private _router: express.Router
    ){
        this.setupRoute();
    }

    private setupRoute():void {
        this._router.get("/:postId", asyncErrHandler(async (req:express.Request, res:express.Response)=> {
            let data = await this._imageQueryService.GetImage(req.params.postId);

            res.writeHead(200,{
                "Content-type": `${data.mimeType}`,
                "Content-Length": data.file.length
            })
            res.end(data.file);

        }));
    }

    public getRoute():express.Router {
        return this._router;
    }



}