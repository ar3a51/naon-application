import * as express     from 'express';
import { asyncErrHandler } from "./asyncErrorHandler";
import * as jsonwebtoken   from 'jsonwebtoken';
import { json } from 'body-parser';

export const authenticateMiddleware = async(req: express.Request, res: express.Response, next: express.NextFunction)=>{
    
  
    if (!req.session!.userId){
        res.status(401).send("unauthorised access")
        return;
    }
    next();
};