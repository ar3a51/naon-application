import * as mongoose from 'mongoose';
import { 
    Schema, 
    Model, 
    model, 
    Document 
}   from "mongoose";
import { Field } from 'type-graphql';

export interface IUser extends Document {
    username: string,
    password: string,
    email:string,
    createDate: Date,
}

export interface IUserDomain {
    username: string,
    password: string,
    email: string,
}

const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    email:String,
    createDate: {
        type: Date,
        default: Date.now,
    },
});

export const UserModel:Model<IUser> = model<IUser>("users",userSchema);