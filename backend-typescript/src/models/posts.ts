import * as mongoose from 'mongoose';
import { 
    Schema, 
    Model, 
    model, 
    Document 
}   from "mongoose";

export interface IPost extends Document {
    username: string,
    message: string,
    subject: string,
    dateTime: Date,
    postType: string,
    location: {
        latitude: string,
        longitude: string,
    },
    attachments: 
        {
            filename: string,
            file: Buffer,
            mimeType: string
        }[]
    

}

export interface IPostDomainModel {
    username: string,
    message: string,
    subject: string,
    postType: string,
    location: {
        latitude: string,
        longitude: string,
    },
    attachments: 
        {
            filename: string,
            file: Buffer,
            mimeType: string
        }[]
    
}

let postsSchema = new mongoose.Schema({
    username: String,
    message: String,
    subject: String,
    dateTime: {
        type: Date,
        default: Date.now,
    },
    postType: String,
    location: {
        latitude: String,
        longitude: String,
    },
    attachments: {
        type: Array,
        "default":[],
    }
});

export const PostModel: Model<IPost> = model<IPost>("Posts",postsSchema);