const path = require('path');

import  mongoose from 'mongoose';
import  express from 'express';
import * as winston from 'winston';
import * as dotenv from 'dotenv';
import * as socketio from 'socket.io';

import {ErrorMiddleware}           from './middlewares/errorMiddleware';
import { ApiStartup }   from './startup/apiStartup';
import { SocketIOStartup }   from './startup/socketIOstartup';

import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { format } from 'path';



//const swaggerDocument = require("./swagger.json");
const app = express();
const router = express.Router();

dotenv.config();

const portNumber = process.env.PORT || 3031;

const logFormat = winston.format.printf(info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`)

winston.configure({
    level: "info",
    format: winston.format.combine(
        winston.format.label({ label: path.basename(process.mainModule.filename) }),
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        // Format the metadata object
        winston.format.metadata({ fillExcept: ['message', 'level', 'timestamp', 'label'] })
      ),
    transports: [
        new winston.transports.File({
            filename: "logfile.log",
            format: winston.format.combine(winston.format.json()),
        }),
    ]
});




process.on("uncaughtException", (err:Error)=>{
    winston.error(err);
    process.exit(1);
});

process.on("unhandledRejection",(err:Error)=>{
    winston.error(err);
    process.exit(1);
})


console.log(`DB Setting: ${process.env.DB_CONNECTION_STRING}`);


mongoose.connect(process.env.DB_CONNECTION_STRING,{useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false}).then( async()=> {
          console.log("DB Connected");
           await startServer();
}).catch(err => {
    console.error("unable to connect to database " + err);
});


//app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const startServer = async () => {
    let http = require('http').Server(app);
   // let io = require('socket.io')(http);

  
    const startExpress:ApiStartup = new ApiStartup(app,router);
    //const startSocket:SocketIOStartup = new SocketIOStartup(io);



    await startExpress.start();
    startExpress.getApolloServer().installSubscriptionHandlers(http);
    //startSocket.startSocket();
   

    http.listen(portNumber,'',() => {
        console.log(`Listening on ${portNumber}`);
        console.log(`subscription ready at ws://localhost:${portNumber}${startExpress.getApolloServer().subscriptionsPath}`)
    });
};

//startServer();