import { ObjectType, Field } from "type-graphql"

@ObjectType()
export class MessageType{
    
    @Field()
    public from: string
    
    @Field()
    public to: string
    
    @Field()
    public message: string
    
    @Field()
    public date: Date
    
    @Field()
    public isRead: boolean

    @Field()
    public id: string
}