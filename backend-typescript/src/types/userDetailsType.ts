import { ObjectType, Field } from "type-graphql";

@ObjectType()
export class UserDetailsType {
    @Field(() => String,  {nullable: true})
    public firstName: string;
    
    @Field(() => String,  {nullable: true})
    public lastName: string;

    /*@Field(() => Buffer)
    public photoProfile: Buffer*/

    @Field(() => String, {nullable: true})
    public address1: string
    
    @Field(()=> String,  {nullable: true})
    public address2: string
    
    @Field(()=> String,  {nullable: true})
    public suburb: string

    @Field(() => String,  {nullable: true})
    public postcode: string
    
    @Field(() => Date,  {nullable: true})
    public createDate: Date
}