import { ObjectType, Field } from "type-graphql";
import { UserDetailsType }   from '../types';

@ObjectType()
export class UserTypes {
   
   @Field(() => String)
   public username: string;

   @Field(() => String)
   public email: string;

   @Field(() => UserDetailsType)
   public userDetails: UserDetailsType
}