import {
    ObjectType,
    Field,
    ClassType,
    Int
} from 'type-graphql';

export default function PaginatedResponse<TItem>(TItemClass: ClassType<TItem>):any {
    // `isAbstract` decorator option is mandatory to prevent registering in schema
    @ObjectType({ isAbstract: true })
     abstract class PaginatedResponseClass {
      @Field(type => [TItemClass])
      items: TItem[];
  
      @Field(type => Int)
      total: number;
    }
    
    return PaginatedResponseClass;
  }