import {
    ObjectType,
    Field,
}   from 'type-graphql';

import {
    UserTypes
}   from '../types'

import { UserDetailsType } from './userDetailsType';
import PaginatedResponse from './pagingType';

@ObjectType()
export class LikeType {
    
    @Field(() => String)
    public postId: string  //if likes for post
    
    @Field(() => String, {nullable: true})
    public replyId: string //if likes for reply

    @Field(() => String)
    public username: string // who gives like
}

@ObjectType()
export class LocationType {

    @Field(()=>String, {nullable: true})
    public latitude: string;

    @Field(()=>String, {nullable: true})
    public longitude: string;
}

@ObjectType()
export class ImageType {
    @Field(() => String)
    public description: string;

    @Field(() => String)
    public extension: string;
}

@ObjectType()
export class ReplyType {

    @Field(() => String)
    public post_id: string;

    @Field(()=>String, {nullable: true})
    public id: string;

    @Field(() => String)
    public message: String;

    @Field(() => String, {nullable: true})
    public parent_reply_id: String;

    @Field(() => Date, {nullable: true})
    public date: Date;

    @Field(()=> String)
    public username: string;

    @Field(() => Date,  {nullable: true})
    public modifiedDate: Date;

    @Field(()=> [ReplyType])
    public childReplies: ReplyType[];
}

@ObjectType()
export class PostType {

    @Field()
    public id: string;

    @Field(()=>String)
    public username: string

    @Field(()=>String)
    public message: string

    @Field(()=>String)
    public postType: string

    @Field(()=>String)
    public subject: string

    @Field(()=>Date)
    public postDate: Date

    @Field(()=>LocationType, {nullable: true})
    public location: LocationType;

    @Field(() => [ReplyType], {nullable: true})
    public replies: ReplyType[];

    @Field()
    public likeCount: number;

    @Field(()=>[LikeType], {nullable: true})
    public likes: LikeType[];

    @Field(()=> [String], {nullable: true})
    public attachments: string[];
}



