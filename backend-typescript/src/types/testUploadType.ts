import { ObjectType, Field, InputType } from "type-graphql";
import {Upload} from './upload';
import { GraphQLUpload } from "graphql-upload";

@InputType()
export class TestType{
    @Field(()=>String)
    public message: String;

    @Field(()=> GraphQLUpload)
    public file: Upload
}