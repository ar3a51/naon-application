import * as express from "express";
import  mongoSanitize from "express-mongo-sanitize";
import  bodyParser from "body-parser";
import cookieParser from 'cookie-parser';
import session from 'express-session';
import mongodbStore from 'connect-mongodb-session';
import 'reflect-metadata';

import { authenticateMiddleware } from "../middlewares/authenticateMiddleware";
import  cors from "cors";

import { ImageQuery } from "../services/imageServices/imageQuery";
import { PostQuery } from "../services/postServices/postQuery";
import { ReplyServiceQuery } from "../services/replyServices/replyServiceQuery";
import { ImageRoute } from "../routes/image/imageRoute";

import { graphQlAuthChecker } from "../middlewares/graphQlAuthChecker";
import { NextHandleFunction } from "connect";
import { ApolloServer } from "apollo-server-express";
import { buildSchema, UnauthorizedError } from "type-graphql";

import {
    LoginResolver,
    MessageResolver,
    PostResolver,
    ReplyResolver,
    UserResolver,

} from '../resolvers';

import winston from "winston/lib/winston/config";
import { setRandomFallback } from "bcryptjs";

const path = require('path');

export class ApiStartup {

  
    private _imageRoute: ImageRoute;
    private _replyQuery:ReplyServiceQuery;
    private _postQuery:PostQuery;

    private _apolloServer:ApolloServer;
    private _whiteList = [];
  

    constructor(
        private _app: express.Express,
        private _route: express.Router
    ){
        _app.use(express.json({limit: '50mb'}));
        //_app.use(cookieParser());
        _app.use(bodyParser.urlencoded({ extended: true}));
        _app.use(mongoSanitize());
        this._whiteList = process.env.ALLOWED_ORIGIN.split(',');
        this.initRoutes(_route);
    }

   private initRoutes(router: express.Router):void {
      
        this._postQuery = new PostQuery();
        let imageQuery: ImageQuery = new ImageQuery();
        this._replyQuery = new ReplyServiceQuery();
        this._imageRoute = new ImageRoute(imageQuery, express.Router());

    }

    public async start() {
    
       
        console.log(`Allowed Origins:`);
        this._whiteList.map(w => console.log('- ',w));
        let corsOpt =  {origin: (origin, callback)=>{
            this.whiteListFilter(origin, callback);
        }, credentials: true}

        let sessionStore = mongodbStore(session);
        let subscriptionPath: string = "/subscription"

        let store = new sessionStore({
            uri: process.env.DB_CONNECTION_STRING,
            collection: "naonSession"
        });

        const sessionMiddleWare = session({
            store: store,
            secret: process.env.SECRET,
            name: "qid",
            resave: false,
            saveUninitialized: false,
            cookie:{
                
                httpOnly: true,
                secure: process.env.NODE_ENV === "production",
                maxAge: 1000 * 60 * 60 * 24 * 7 * 365,
                sameSite: process.env.NODE_ENV === "production",
            },

        });

        this._app.use(cors(
            corsOpt
        ));

       
        this._app.use(sessionMiddleWare);
        this._app.use("/",express.static(path.resolve('www')));
        this._app.get("/test", (req, res)=>{
           
           res.end(`
                hostname is : ${req.hostname}
                IP : ${req.ip}
                protocol: ${req.headers["x-forwarded-proto"]}
           `)
        });

        this._apolloServer = new ApolloServer({
            schema: await buildSchema({
                resolvers:[path.join(process.cwd(),"/src/resolvers/**/*.resolver.{ts,js}"),
                            path.join(process.cwd(),"/resolvers/**/*.resolver.{ts,js}"),
                            path.join(process.cwd(),"/dist/resolvers/**/*.resolver.{ts,js}")    ],
                authChecker: graphQlAuthChecker
            
            }),
            cacheControl: {
                defaultMaxAge: 0,
            },
            subscriptions:{
                path: subscriptionPath,
                onConnect:(_, ws:any)=> {
                    
                    return new Promise((res,rej) => 
                        sessionMiddleWare(ws.upgradeReq,{} as any, ()=>{
                            if (!ws.upgradeReq.session.userId){
                               throw new UnauthorizedError();
                            } else {
                                res({req: ws.upgradeReq})
                            }
                        })
                    )
                },
            },
            context:({req, res, connection}) => {
              
                return {
                    replyLoader: this._replyQuery.getReplyLoader(),
                    likeLoader: this._postQuery.getLikerLoader(),
                    req,
                    res,
                    connection
                }
            },
            playground:{
                subscriptionEndpoint: subscriptionPath,
            }
        });

        this._app.use("/image",authenticateMiddleware, this._imageRoute.getRoute());
       
        this._apolloServer.applyMiddleware({app: this._app, cors: corsOpt, path:"/graphql"});
       
        
        
    }

     private whiteListFilter(origin, callback): void {
         //let whiteList =  process.env.ALLOWED_ORIGIN.split(',');
         if (!origin){
             callback(null,true);
             return;
         }
        let avail = this._whiteList.filter(orig => {
            return origin.indexOf(orig) > -1}
            )[0];
        if (avail){
         
            callback(null,true);
        } else {
            callback(new Error("Not a valid origin"))
        }
            
    }

    getApolloServer():ApolloServer {
        return this._apolloServer;
    }


}