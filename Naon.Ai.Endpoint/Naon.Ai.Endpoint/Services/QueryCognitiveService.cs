﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

using Microsoft.Extensions.Options;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;

using Naon.Ai.Endpoint.Models;
using Naon.Ai.Endpoint.Models.DocumentModels;

namespace Naon.Ai.Endpoint.Services
{
    public class QueryCognitiveService : IQueryCognitiveService
    {
        private readonly ComputerVisionClient _computerVisionClient;
        
        public QueryCognitiveService(IOptions<AppSettings> options)
        {
            _computerVisionClient = new ComputerVisionClient(
               new ApiKeyServiceClientCredentials(options.Value.CognitiveApiKey),
               new System.Net.Http.DelegatingHandler[] { });

            _computerVisionClient.Endpoint = options.Value.ComputerVisionEndPoint;

           

        }
        public async Task<ImageAnalysis> QueryService(PostDocument post)
        {
           
            Stream stream = new MemoryStream(post.Image.ImageFile);
            ImageAnalysis analysis = await _computerVisionClient.AnalyzeImageInStreamAsync(stream, new List<VisualFeatureTypes>() {
                VisualFeatureTypes.Description,
                VisualFeatureTypes.Tags,
                VisualFeatureTypes.Objects
            });
            
            return analysis;
        }
    }
}
